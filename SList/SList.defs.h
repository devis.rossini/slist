#ifndef SLIST_DEFS_H_INCLUDE_GUARD
#define SLIST_DEFS_H_INCLUDE_GUARD

#include "SList.commons.h"

#include <iterator>

namespace sll
{
	template <typename T> 
	struct slist_node 
	{
		slist_node(const T& v, slist_node* n) 
			: value(v), next(n) { }
	
		T value;
		slist_node* next;
	};

	template <typename T, bool isconst = false> 
	struct slist_iterator {

		typedef std::forward_iterator_tag								iterator_category;

		typedef T														value_type;
		
		typedef std::ptrdiff_t											difference_type;

		typedef typename _const_behavior<isconst, const T&, T&>::type	reference;
		typedef typename _const_behavior<isconst, const T*, T*>::type	pointer;

		typedef typename _const_behavior<isconst, const slist_node<T>&,    
			slist_node<T>&>::type node_ref;

		typedef typename _const_behavior<isconst, const slist_node<T>*,    
			slist_node<T>*>::type node_ptr;

		slist_iterator(node_ptr p = 0)
			: node(p) { }

		slist_iterator(const slist_iterator<T, false>& it) 
			: node(it.node) { }

		reference operator*() const 
		{ 
			return node->value; 
		}

		pointer operator->() const 
		{ 
			return &(node->value); 
		}

		slist_iterator& operator++() 
		{ 
			node = node->next; 
			return *this; 
		}

		slist_iterator operator++(int) 
		{
			slist_iterator tmp(*this);
			++*this;
			return tmp;
		}

		friend bool operator==(const slist_iterator& it1, const slist_iterator& it2) 
		{
			return (it1.node == it2.node);
		}

		friend bool operator!=(const slist_iterator& it1, const slist_iterator& it2) 
		{
			return (it1.node != it2.node);
		}

		node_ptr node;
	};
}

#endif // SLIST_DEFS_H_INCLUDE_GUARD