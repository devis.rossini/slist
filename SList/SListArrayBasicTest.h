#ifndef SLISTARRAYBASICTEST_H_INCLUDE_GUARD
#define SLISTARRAYBASICTEST_H_INCLUDE_GUARD

namespace sll
{
	/*
		Basic benchmark for the SListArray class.
		The different tests perform the following operations:

		(o) TEST 01 :	SListArray()
						empty()
						size()
						push_front()
						push_back()
						pop_front()
						pop_back()

		(o) TEST 02 :	SListArray(n, const T& = T())
						SListArray(const SListArray&)

		(o) TEST 03 :	SListArray()
						push_front()
						empty()
						front()
						back()

		(o) TEST 04 :	SListArray(n, const T& = T())
						size()
						insert(pos, const T&)
						front()
						back()						
	*/
	void SListArrayBasicTest(bool, bool, bool, bool);
}

#endif // SLISTARRAYBASICTEST_H_INCLUDE_GUARD