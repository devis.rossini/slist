#include "FixedSListBasicTest.h"
#include "FixedSList.h"

#include <iostream>
#include <cassert>

namespace sll
{
	void FixedSListBasicTest(bool executeTest1 = true, bool executeTest2 = true, bool executeTest3 = true, 
		bool executeTest4 = true)
	{
		std::cout << "************************ FixedSListBasicTest() ************************" << std::endl << std::endl;

		#pragma region TEST 01 
		std::cout << "******************************* TEST 01 *******************************" << std::endl << std::endl;
		if (executeTest1) 
		{
			const int N = 30;

			std::cout << "Default constructor...";
			FixedSList<int, N> l1;
			std::cout << "OK. SList<int> l1 created." << std::endl;

			std::cout << "Assertion (l1.empty()): ";
			assert(l1.empty());
			std::cout << "OK." << std::endl;
			std::cout << "Assertion (l1.size() == 0): ";
			assert(l1.size() == 0);
			std::cout << "OK." << std::endl;

			std::cout << "Try to print l1...";
			for (FixedSList<int, 30>::const_iterator it = l1.begin(); it != l1.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;

			for (size_t i = 0; i < 10; ++i)
			{
				std::cout << "Insert operation: push_front(" << i << ")...";
				l1.push_front(i);
				std::cout << "OK." << std::endl;
			}

			std::cout << "Assertion (!l1.empty()): ";
			assert(!l1.empty());
			std::cout << "OK." << std::endl;

			std::cout << "Current l1 size: " << l1.size() << std::endl;

			for (size_t i = 0; i < 10; ++i)
			{
				std::cout << "Insert operation: push_back(" << i << ")...";
				l1.push_back(i);
				std::cout << "OK." << std::endl;
			}

			std::cout << "Current l1 size: " << l1.size() << std::endl;

			std::cout << "Try to print l1...";
			for (FixedSList<int, N>::const_iterator it = l1.begin(); it != l1.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;

			for (size_t i = 0; i < 10; ++i)
			{
				std::cout << "Removal operation: pop_front()...";
				l1.pop_front();
				std::cout << "OK." << std::endl;

				std::cout << "Try to print l1...";
				for (FixedSList<int, N>::const_iterator it = l1.begin(); it != l1.end(); ++it)
					std::cout << *it << " ";
				std::cout << "OK." << std::endl;

				std::cout << "Removal operation: pop_back()...";
				l1.pop_back();
				std::cout << "OK." << std::endl;

				std::cout << "Try to print l1...";
				for (FixedSList<int, N>::const_iterator it = l1.begin(); it != l1.end(); ++it)
					std::cout << *it << " ";
				std::cout << "OK." << std::endl;

				std::cout << "Current l1 size: " << l1.size() << std::endl;
			}

			std::cout << "Assertion (l1.empty()): ";
			assert(l1.empty());
			std::cout << "OK." << std::endl;
		}
		std::cout << "************************** TEST 01 COMPLETED **************************" << std::endl << std::endl;
		#pragma endregion	

		#pragma region TEST 02
		std::cout << "******************************* TEST 02 *******************************" << std::endl << std::endl;
		if (executeTest2) 
		{
			const int N = 20;

			std::cout << "Constructor...";
			FixedSList<int, N> l2(5, 3);
			std::cout << "OK. SList<int> l2 created." << std::endl;

			std::cout << "Try to print l2...";
			for (FixedSList<int, N>::const_iterator it = l2.begin(); it != l2.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;

			std::cout << "Copy constructor...";
			FixedSList<int, N> l3(l2);
			std::cout << "OK. SList<int> l3(l2) created." << std::endl;

			std::cout << "Try to print l3...";
			for (FixedSList<int, N>::const_iterator it = l3.begin(); it != l3.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;
		}
		std::cout << "************************** TEST 02 COMPLETED **************************" << std::endl << std::endl;
		#pragma endregion

		#pragma region TEST 03
		std::cout << "******************************* TEST 03 *******************************" << std::endl << std::endl;
		if (executeTest3) 
		{
			const int N = 20;

			std::cout << "Default constructor...";
			FixedSList<int, N> l4;
			std::cout << "OK. SList<int> l4 created." << std::endl;

			for (size_t i = 0; i < 10; ++i)
			{
				std::cout << "Insert operation: push_front(" << i << ")...";
				l4.push_front(i);
				std::cout << "OK." << std::endl;
			}

			std::cout << "Assertion (!l4.empty()): ";
			assert(!l4.empty());
			std::cout << "OK." << std::endl;

			std::cout << "Current l1 size: " << l4.size() << std::endl;

			std::cout << "Try to print l4...";
			for (FixedSList<int, N>::const_iterator it = l4.begin(); it != l4.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;

			std::cout << "Element access (front()): " << l4.front() << " OK." << std::endl;
			std::cout << "Element access (back()): " << l4.back() << " OK." << std::endl;

			std::cout << "Element access (l4.front() = 0): ";
			l4.front() = 0;
			assert(l4.front() == 0);
			std::cout << "OK." << std::endl;

			std::cout << "Element access (l4.back() = 0): ";
			l4.back() = 0;
			assert(l4.back() == 0);
			std::cout << "OK." << std::endl;

			std::cout << "Element access (front()): " << l4.front() << " OK." << std::endl;
			std::cout << "Element access (back()): " << l4.back() << " OK." << std::endl;
		}
		std::cout << "************************** TEST 03 COMPLETED **************************" << std::endl << std::endl;
		#pragma endregion

		#pragma region TEST 04
		std::cout << "******************************* TEST 04 *******************************" << std::endl << std::endl;
		if (executeTest4)
		{
			const int N = 20;

			std::cout << "Constructor...";
			FixedSList<int, N> l5(10, -1);
			std::cout << "OK. SList<int> l5 created." << std::endl;

			std::cout << "Assertion (l5.size() == 10): ";
			assert(l5.size() == 10);
			std::cout << "OK." << std::endl;

			std::cout << "Try to print l5...";
			for (FixedSList<int, N>::const_iterator it = l5.begin(); it != l5.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;

			std::cout << "Insert operation (insert(l5.begin(), 2)...";
			l5.insert(l5.begin(), 2);
			std::cout << "OK." << std::endl;

			std::cout << "Assertion (l5.front() == 2): ";
			assert(l5.front() == 2);
			std::cout << "OK." << std::endl;

			std::cout << "Assertion (l5.size() == 11): ";
			assert(l5.size() == 11);
			std::cout << "OK." << std::endl;

			std::cout << "Try to print l5...";
			for (FixedSList<int, N>::const_iterator it = l5.begin(); it != l5.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;

			std::cout << "Insert operation (insert(l5.end(), 2)...";
			l5.insert(l5.end(), 2);
			std::cout << "OK." << std::endl;

			std::cout << "Assertion (l5.back() == 2): ";
			assert(l5.back() == 2);
			std::cout << "OK." << std::endl;

			std::cout << "Assertion (l5.size() == 12): ";
			assert(l5.size() == 12);
			std::cout << "OK." << std::endl;

			std::cout << "Try to print l5...";
			for (FixedSList<int, N>::const_iterator it = l5.begin(); it != l5.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;

			std::cout << "Try to reach the middle of the list...";
			FixedSList<int, N>::iterator middle = l5.begin();
			for (size_t i = 0; i < 6; ++i)
				++middle;
			std::cout << "OK." << std::endl;

			std::cout << "Insert operation (insert(l5.begin() + 6, 2)...";
			middle = l5.insert(middle, 2);
			std::cout << "OK." << std::endl;

			std::cout << "Assertion (*middle == 2): ";
			assert(*middle == 2);
			std::cout << "OK." << std::endl;

			std::cout << "Assertion (l5.size() == 13): ";
			assert(l5.size() == 13);
			std::cout << "OK." << std::endl;

			std::cout << "Try to print l5...";
			for (FixedSList<int, N>::const_iterator it = l5.begin(); it != l5.end(); ++it)
				std::cout << *it << " ";
			std::cout << "OK." << std::endl;
		}
		std::cout << "************************** TEST 04 COMPLETED **************************" << std::endl << std::endl;
		#pragma endregion
	}
}