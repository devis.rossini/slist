#ifndef SLISTARRAY_DEFS_H_INCLUDE_GUARD
#define SLISTARRAY_DEFS_H_INCLUDE_GUARD 

#include "SList.commons.h"

#include <iterator>
#include <vector>

namespace sll
{
	template<typename T>
	struct slistarray_node
	{
		typedef T													value_type;
		typedef typename std::vector<slistarray_node>::size_type	size_type;

		slistarray_node(const T& v, size_type n)
			: value(v), next(n) { }

		value_type value;
		size_type next;
	};

	template<typename T>
	class SListArray;

	template <typename T, bool isconst = false> 
	struct slistarray_iterator {

		typedef std::forward_iterator_tag								iterator_category;

		typedef T														value_type;

		typedef typename std::vector<T>::difference_type				difference_type;
		
		typedef typename _const_behavior<isconst, const T&, T&>::type	reference;
		typedef typename _const_behavior<isconst, const T*, T*>::type	pointer;

		typedef typename _const_behavior<isconst, const slistarray_node<T>*,    
			slistarray_node<T>*>::type node_ptr;

		typedef typename _const_behavior<isconst, const slistarray_node<T>&,    
			slistarray_node<T>&>::type node_ref;

		typedef typename _const_behavior<isconst, const SListArray<T>*,    
			SListArray<T>*>::type list_ptr;

		typedef typename _const_behavior<isconst, const SListArray<T>&,    
			SListArray<T>&>::type list_ref;

		typedef typename std::vector<slistarray_node<T> >::size_type index_type;

		slistarray_iterator(list_ptr const l = 0, index_type i = 0)
			: lst(l), index(i) { }

		slistarray_iterator(const slistarray_iterator<T, false>& it) 
			: lst(it.lst), index(it.index) { }

		reference operator*() const 
		{ 
			return get_node().value;
		}

		pointer operator->() const 
		{ 
			return &(get_node().value); 
		}

		slistarray_iterator& operator++() 
		{ 
			index = ((*lst).m_vector.at(index).next); 
			return *this; 
		}

		slistarray_iterator operator++(int) 
		{
			slistarray_iterator tmp(*this);
			++*this;
			return tmp;
		}

		node_ref get_node() const
		{
			return ((*lst).m_vector.at(index));
		}

		index_type get_index() const
		{
			return index;
		}

		friend bool operator==(const slistarray_iterator& it1, const slistarray_iterator& it2) 
		{
			return (it1.index == it2.index);
		}

		friend bool operator!=(const slistarray_iterator& it1, const slistarray_iterator& it2) 
		{
			return (it1.index != it2.index);
		}

		list_ptr lst;
		index_type index;
	};
}

#endif // SLISTARRAY_DEFS_H_INCLUDE_GUARD