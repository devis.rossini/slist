#ifndef SLISTARRAY_H_INCLUDE_GUARD
#define SLISTARRAY_H_INCLUDE_GUARD

#include "SListArray.defs.h"

#include <memory>
#include <vector>

namespace sll
{
	template<typename T>
	class SListArray
	{
	public:
		/* Types */
		#pragma region Types

		typedef T															value_type;
	
		typedef typename std::vector<slistarray_node<T> >::size_type		size_type;

		typedef typename std::vector<slistarray_node<T> >::difference_type	difference_type;

		typedef slistarray_iterator<T>										iterator;
		typedef slistarray_iterator<T, true>								const_iterator;

		typedef typename T*													pointer;
		typedef typename const T*											const_pointer;

		typedef typename T&													reference;
		typedef typename const T&											const_reference;

		typedef slistarray_node<T>											node_type;

		typedef slistarray_node<T>*											node_ptr;
		typedef const slistarray_node<T>*									node_const_ptr;

		typedef slistarray_node<T>&											node_ref;
		typedef const slistarray_node<T>&									node_const_ref;

		#pragma endregion

		/* Iterators */
		#pragma region Iterators

		iterator begin();
		const_iterator begin() const;

		iterator end();
		const_iterator end() const;

		#pragma endregion

		/* Constructors */
		#pragma region Constructors and destructor

		explicit SListArray();
		explicit SListArray(size_type n, const T& value = T());
			
		SListArray(const SListArray& l);
		SListArray& operator=(const SListArray& l);

		// template<typename In>
		// SListArray(In first, In last, const A& a = A());

		~SListArray();

		#pragma endregion

		/* Access to elements */
		#pragma region Access to elements

		reference front();
		const_reference front() const;

		reference back();
		const_reference back() const;

		#pragma endregion

		/* Stack/Queue operations */
		#pragma region Stack/Queue operations

		void push_front(const T& value);
		void pop_front();

		void push_back(const T& value);
		void pop_back();

		#pragma endregion

		/* List operations */
		#pragma region List operations

		iterator insert(iterator pos, const T& value);
		// void insert(iterator pos, size_type n, const T& value);

		// template<typename In>
		// void insert(iterator pos, In first, In last);

		iterator erase(iterator pos);
		iterator erase(iterator first, iterator last);

		void clear();
		
		#pragma endregion

		/* Size and capacity */
		#pragma region Size and capacity

		size_type size() const;

		bool empty() const;
		
		// void resize(size_type sz, T value = T());

		#pragma endregion

		/* Utilities */
		#pragma region Utilities

		template<typename In>
		void assign(In first, In last);
		// void assign(size_type n, const T& value);

		void swap(SListArray& l);

		// HINT (FUTURE DEVELOPMENT): It's possible to automatically use 
		// this function when we reach a certain number of removal r. 
		// A good choice could be r = size() / 2.
		void reallocate();

		#pragma endregion

		friend struct slistarray_iterator<T, false>;
		friend struct slistarray_iterator<T, true>;

	private:
		#pragma region Private utilities

		iterator get_tail();

		#pragma endregion

		size_type				m_head;
		std::vector<node_type>	m_vector;
	};
}

#include "SListArray.inlines.h"
#include "SListArray.templates.h"

#endif // SLISTARRAY_H_INCLUDE_GUARD