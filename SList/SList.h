#ifndef SLIST_H_INCLUDE_GUARD
#define SLIST_H_INCLUDE_GUARD

#include "SList.defs.h"

#include <memory>

namespace sll
{
	template<typename T, typename A = std::allocator<slist_node<T> > >
	class SList 
	{
	public:
		/* Types */
		#pragma region Types

		typedef T													value_type;
		typedef A													allocator_type;
		typedef typename A::size_type								size_type;
		typedef typename A::difference_type							difference_type;

		typedef slist_iterator<T>									iterator;
		typedef slist_iterator<T, true>								const_iterator;

		typedef typename T*											pointer;
		typedef typename const T*									const_pointer;

		typedef typename T&											reference;
		typedef typename const T&									const_reference;

		typedef slist_node<T>										node_type;
		
		typedef slist_node<T>*										node_ptr;
		typedef const slist_node<T>*								node_const_ptr;

		typedef slist_node<T>&										node_ref;
		typedef const slist_node<T>&								node_const_ref;

		#pragma endregion

		/* Iterators */
		#pragma region Iterators

		iterator begin();
		const_iterator begin() const;

		iterator end();
		const_iterator end() const;

		#pragma endregion

		/* Constructors */
		#pragma region Constructors and destructor

		explicit SList(const A& a = A());
		explicit SList(size_type n, const T& value = T(), const A& a = A());

		SList(const SList& l);
		SList& operator=(const SList& l);

		// template<typename In>
		// SList(In first, In last, const A& a = A());

		~SList();
		
		#pragma endregion

		/* Access to elements */
		#pragma region Access to elements

		reference front();
		const_reference front() const;

		reference back();
		const_reference back() const;

		#pragma endregion

		/* Stack/Queue operations */
		#pragma region Stack/Queue operations

		void push_back(const T& value);
		void pop_back();
		
		void push_front(const T& value);
		void pop_front();

		#pragma endregion

		/* List operations */
		#pragma region List operations

		iterator insert(iterator pos, const T& value);
		// void insert(iterator pos, size_type n, const T& value);

		// template<typename In>
		// void insert(iterator pos, In first, In last);

		iterator erase(iterator pos);
		iterator erase(iterator first, iterator last);

		void clear();

		#pragma endregion

		/* Size and capacity */
		#pragma region Size and capacity

		size_type size() const;

		bool empty() const;

		// void resize(size_type sz, T value = T());

		#pragma endregion

		/* Utilities */
		#pragma region Utilities

		allocator_type get_allocator() const;

		void swap(SList& l);

		#pragma endregion

	private:
		#pragma region Private utilities

		node_ptr allocate_and_create(const T& value, node_ptr next = 0);
		node_ptr destroy_and_deallocate(node_ptr p);
		
		iterator get_tail();

		#pragma endregion
		
		A m_alloc;
		node_ptr m_head;
		size_type m_size;
	};
}

#include "SList.inlines.h"
#include "SList.templates.h"

#endif // SLIST_H_INCLUDE_GUARD