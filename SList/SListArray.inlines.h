#ifndef SLISTARRAY_INLINES_H_INCLUDE_GUARD
#define SLISTARRAY_INLINES_H_INCLUDE_GUARD

#include <algorithm>
#include <iterator>
#include <cassert>
#include <limits>

namespace sll
{
	#pragma region Iterators

	template<typename T>
	inline
	typename SListArray<T>::iterator SListArray<T>::begin() 
	{
		return iterator(this, m_head);
	}

	template<typename T>
	inline
	typename SListArray<T>::const_iterator SListArray<T>::begin() const
	{
		return const_iterator(this, m_head);
	}

	template<typename T>
	inline
	typename SListArray<T>::iterator SListArray<T>::end()
	{
		return iterator(this, std::numeric_limits<size_type>::max());
	}

	template<typename T>
	inline
	typename SListArray<T>::const_iterator SListArray<T>::end() const
	{
		return const_iterator(this, std::numeric_limits<size_type>::max());
	}

	#pragma endregion

	#pragma region Constructors and destructor

	template<typename T>
	inline
	SListArray<T>::SListArray()
		:	m_head(std::numeric_limits<size_type>::max()), 
			m_vector(std::vector<node_type>()) { }

	template<typename T>
	inline
	SListArray<T>::SListArray(size_type n, const T& value)
		:	m_head(std::numeric_limits<size_type>::max()), 
			m_vector(std::vector<node_type>())
	{
		for (size_type i = 0; i < n; ++i)
			push_front(value);
	}

	template<typename T>
	inline
	SListArray<T>::SListArray(const SListArray& l)
		:	m_head(std::numeric_limits<size_type>::max()), 
			m_vector(std::vector<node_type>())
	{
		iterator it = begin();
		std::copy(l.begin(), l.end(), std::inserter(*this, it));
	}

	template<typename T>
	inline
	SListArray<T>& SListArray<T>::operator=(const SListArray& l)
	{
		if (this == &l) return *this;

		clear();
		iterator it = begin();
		std::copy(l.begin(), l.end(), std::inserter(*this, it));
	}

	template<typename T>
	inline 
	SListArray<T>::~SListArray() { }

	#pragma endregion

	#pragma region Access to elements

	template<typename T>
	inline 
	typename SListArray<T>::reference SListArray<T>::front()
	{
		assert(!empty());
		return *begin();
	}

	template<typename T>
	inline
	typename SListArray<T>::const_reference SListArray<T>::front() const
	{
		assert(!empty());
		return *begin();
	}

	template<typename T>
	inline 
	typename SListArray<T>::reference SListArray<T>::back()
	{
		assert(!empty());
		iterator tail = get_tail();
		return *tail;
	}

	template<typename T>
	inline
	typename SListArray<T>::const_reference SListArray<T>::back() const
	{
		assert(!empty());
		iterator tail = get_tail();
		return *tail;
	}

	#pragma endregion

	#pragma region Stack/Queue operations

	template<typename T>
	inline
	void SListArray<T>::push_front(const T& value)
	{
		m_vector.push_back(node_type(value, m_head)) ; 
		m_head = m_vector.size() - 1;
	}

	template<typename T>
	inline
	void SListArray<T>::pop_front()
	{
		assert(!empty());
		m_head = m_vector.at(m_head).next;
	}

	#pragma endregion 

	#pragma region List operations

	template<typename T>
	inline 
	void SListArray<T>::clear()
	{
		erase(begin(), end());
	}

	#pragma endregion

	#pragma region Size and capacity

	template<typename T>
	inline
	typename SListArray<T>::size_type SListArray<T>::size() const
	{
		size_type sz = 0;

		const_iterator it = begin();
		while(it != end())
		{
			++sz;
			++it;
		}
		return sz;
	}

	template<typename T>
	inline
	bool SListArray<T>::empty() const
	{
		return (begin() == end());
	}

	#pragma endregion

	#pragma region Utilities

	template<typename T>
	inline
	void SListArray<T>::swap(SListArray& l)
	{		
		std::swap(m_head,	l.m_head);
		std::swap(m_vector, l.m_vector);
	}

	#pragma endregion
}

#endif // SLISTARRAY_INLINES_H_INCLUDE_GUARD