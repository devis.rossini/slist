#ifndef SLISTARRAY_TEMPLATES_H_INCLUDE_GUARD
#define SLISTARRAY_TEMPLATES_H_INCLUDE_GUARD

#include <algorithm>
#include <cassert>

namespace sll
{
	#pragma region Stack/Queue operations

	template<typename T>
	void SListArray<T>::push_back(const T& value)
	{
		if (empty())
		{
			push_front(value);
		}
		else 
		{
			insert(end(), value);
		}
	}

	template<typename T>
	void SListArray<T>::pop_back()
	{
		assert(!empty());

		if (size() == 1)
		{
			pop_front();
		}
		else 
		{
			iterator tail = get_tail();
			erase(tail);
		}
	}

	#pragma endregion

	#pragma region List operations

	template<typename T>
	typename SListArray<T>::iterator SListArray<T>::insert(iterator pos, const T& value)
	{
		if (pos == begin())
		{
			push_front(value);
			return begin();
		}
		else 
		{
			iterator prev = begin();
			iterator curr = prev;
			++curr;

			while (curr != pos)
			{
				++prev; 
				++curr;
			}

			m_vector.push_back(node_type(value, curr.get_index()));
			prev.get_node().next = m_vector.size() - 1;

			return iterator(this, m_vector.size() - 1);
		}

		return end();
	}

	template<typename T>
	typename SListArray<T>::iterator SListArray<T>::erase(iterator pos)
	{
		if (pos == end())
			return end();

		if (pos == begin())
		{
			pop_front();
			return begin();
		}

		iterator prev = begin();
		iterator curr = prev;
		++curr;

		while (curr != pos)
		{
			++prev;
			++curr;
		}

		size_type next = curr.get_node().next;
		prev.get_node().next = next;

		return iterator(this, next);
	}

	template<typename T>
	typename SListArray<T>::iterator SListArray<T>::erase(iterator first, iterator last)
	{
		iterator ret = end();
		iterator tmp = first;

		while (tmp != last)
		{
			iterator next = tmp;
			++next;

			ret = erase(tmp);
			tmp = next;
		}

		return ret;
	}

	#pragma endregion

	#pragma region Utilities

	template<typename T>
	template<typename In> 
	void SListArray<T>::assign(In first, In last)
	{
		clear();

		for (In it = first; it != last; ++it)
		{
			push_back(*it);
		}
	}

	template<typename T>
	void SListArray<T>::reallocate() 
	{
		SListArray<T> tmp;
		tmp.assign(begin(), end());

		std::swap(m_head,	tmp.m_head);
		std::swap(m_vector, tmp.m_vector);
	}

	#pragma endregion

	#pragma region Private utilities

	template<typename T>
	typename SListArray<T>::iterator SListArray<T>::get_tail()
	{
		iterator tail = begin();

		if (tail != end())
		{
			iterator next = tail;
			++next;

			while (next != end())
			{
				++tail;
				++next;
			}
		}

		return tail;
	}

	#pragma endregion
}

#endif // SLISTARRAY_TEMPLATES_H_INCLUDE_GUARD