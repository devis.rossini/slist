#ifndef FIXEDSLIST_H_INCLUDE_GUARD
#define FIXEDSLIST_H_INCLUDE_GUARD

#include "FixedSList.defs.h"

#include <memory>

namespace sll
{
	template<typename T, unsigned int N, typename A = std::allocator<fixedslist_node<T> > >
	class FixedSList
	{
	public:
		/* Types */
		#pragma region Types

		typedef T													value_type;
		typedef A													allocator_type;
		typedef typename A::size_type								size_type;
		typedef typename A::difference_type							difference_type;

		typedef fixedslist_iterator<T>								iterator;
		typedef fixedslist_iterator<T, true>						const_iterator;

		typedef typename T*											pointer;
		typedef typename const T*									const_pointer;

		typedef typename T&											reference;
		typedef typename const T&									const_reference;

		typedef fixedslist_node<T>									node_type;

		typedef fixedslist_node<T>*									node_ptr;
		typedef const fixedslist_node<T>*							node_const_ptr;

		typedef fixedslist_node<T>&									node_ref;
		typedef const fixedslist_node<T>&							node_const_ref;

		#pragma endregion

		/* Iterators */
		#pragma region Iterators

		iterator begin();
		const_iterator begin() const;
		
		iterator end();
		const_iterator end() const;
		
		#pragma endregion

		/* Constructors */
		#pragma region Constructors and destructor

		explicit FixedSList(const A& a = A());
		explicit FixedSList(size_type n, const T& value = T(), const A& a = A());

		FixedSList(const FixedSList& l);
		FixedSList& operator=(const FixedSList& l);
		
		// template<typename In>
		// FixedSList(In first, In last, const A& a = A());

		~FixedSList();

		#pragma endregion

		/* Access to elements */
		#pragma region Access to elements

		reference front();
		const_reference front() const;
		
		reference back();
		const_reference back() const;
		
		#pragma endregion

		/* Stack/Queue operations */
		#pragma region Stack/Queue operations

		void push_front(const T& value);
		void pop_front();

		void push_back(const T& value);
		void pop_back();
		
		#pragma endregion

		/* List operations */
		#pragma region List operations

		iterator insert(iterator pos, const T& value);
		// void insert(iterator pos, size_type n, const T& value);

		// template<typename In>
		// void insert(iterator pos, In first, In last);

		iterator erase(iterator pos);
		iterator erase(iterator first, iterator last);

		void clear();
		
		#pragma endregion

		/* Size and capacity */
		#pragma region Size and capacity

		size_type size() const;

		bool empty() const;
	
		// void resize(size_type sz, T value = T());

		#pragma endregion

		/* Utilities */
		#pragma region Utilities

		void swap(FixedSList& l);

		#pragma endregion

	private:
		#pragma region Private utilities

		iterator get_tail();

		node_ptr empty_list();

		#pragma endregion

		A			m_alloc;
		node_ptr	m_lst;
		size_type	m_head;
		size_type	m_free;
	};
}

#include "FixedSList.inlines.h"
#include "FixedSList.templates.h"

#endif // FIXEDSLIST_H_INCLUDE_GUARD