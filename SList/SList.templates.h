#ifndef SLIST_TEMPLATES_H_INCLUDE_GUARD
#define SLIST_TEMPLATES_H_INCLUDE_GUARD

#include <cassert>

namespace sll
{
	#pragma region Stack/Queue operations

	template<typename T, typename A>
	void SList<T, A>::push_back(const T& value)
	{
		if (empty())
		{
			push_front(value);
		}
		else 
		{
			insert(end(), value);
		}
	}

	template<typename T, typename A>
	void SList<T, A>::pop_back()
	{
		assert(!empty());

		if (m_size == 1)
		{
			pop_front();
		}
		else 
		{
			iterator tail = get_tail();
			erase(tail);
		}
	}

	#pragma endregion

	#pragma region List operations

	template<typename T, typename A>
	typename SList<T, A>::iterator SList<T, A>::insert(iterator pos, const T& value)
	{
		if (pos == begin())
		{
			push_front(value);
			return begin();
		}
		else 
		{
			iterator prev = begin();
			iterator curr = prev;
			++curr;

			while (curr != pos)
			{
				++prev; 
				++curr;
			}

			node_ptr prev_ptr = prev.node;
			prev_ptr->next = allocate_and_create(value, curr.node);
			++m_size;

			return iterator(prev_ptr->next);
		}

		return end();
	}

	template<typename T, typename A>
	typename SList<T, A>::iterator SList<T, A>::erase(iterator pos)
	{
		if (pos == end())
			return end();

		if (pos == begin())
		{
			pop_front();
			return begin();
		}

		iterator prev = begin();
		iterator curr = prev;
		++curr;

		while (curr != pos)
		{
			++prev;
			++curr;
		}

		node_ptr prev_ptr = prev.node;
		prev_ptr->next = destroy_and_deallocate(curr.node);
		--m_size;

		return iterator(prev_ptr->next);
	}

	template<typename T, typename A>
	typename SList<T, A>::iterator SList<T, A>::erase(iterator first, iterator last)
	{
		iterator ret = end();
		iterator tmp = first;

		while (tmp != last)
		{
			iterator next = tmp;
			++next;

			ret = erase(tmp);
			tmp = next;
		}

		return ret;
	}

	#pragma endregion

	#pragma region Private utilities

	template<typename T, typename A>
	typename SList<T, A>::node_ptr SList<T, A>::allocate_and_create(const T& value, node_ptr next)
	{
		node_type new_node(value, next);
		node_ptr p = m_alloc.allocate(1);
		m_alloc.construct(p, new_node);
		return p;
	}
	
	template<typename T, typename A>
	typename SList<T, A>::node_ptr SList<T, A>::destroy_and_deallocate(node_ptr p)
	{
		node_ptr tmp = p->next;
		m_alloc.destroy(p);
		m_alloc.deallocate(p, 1);	
		return tmp;
	}

	template<typename T, typename A>
	typename SList<T, A>::iterator SList<T, A>::get_tail()
	{
		iterator tail = begin();

		if (tail != end())
		{
			iterator next = tail;
			++next;

			while (next != end())
			{
				++tail;
				++next;
			}
		}

		return tail;
	}

	#pragma endregion
}

#endif // SLIST_TEMPLATES_H_INCLUDE_GUARD