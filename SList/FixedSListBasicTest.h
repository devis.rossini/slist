#ifndef FIXEDSLISTBASICTEST_H_INCLUDE_GUARD
#define FIXEDSLISTBASICTEST_H_INCLUDE_GUARD

namespace sll
{
	/*
		Basic benchmark for the FixedSList class.
		The different tests perform the following operations:

		(o) TEST 01 :	FixedSList()
						empty()
						size()
						push_front()
						push_back()
						pop_front()
						pop_back()

		(o) TEST 02 :	FixedSList(n, const T& = T())
						FixedSList(const FixedSList&)

		(o) TEST 03 :	FixedSList()
						push_front()
						empty()
						front()
						back()

		(o) TEST 04 :	FixedSList(n, const T& = T())
						size()
						insert(pos, const T&)
						front()
						back()						
	*/
	void FixedSListBasicTest(bool, bool, bool, bool);
}

#endif // FIXEDSLISTBASICTEST_H_INCLUDE_GUARD