#include "SListBasicTest.h"
#include "SListArrayBasicTest.h"
#include "FixedSListBasicTest.h"

using namespace sll;

int main()
{
	SListBasicTest(true, true, true, true);
	SListArrayBasicTest(true, true, true, true);
	FixedSListBasicTest(true, true, true, true);

	return 0;
}