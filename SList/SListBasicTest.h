#ifndef SLISTBASICTEST_H_INCLUDE_GUARD
#define SLISTBASICTEST_H_INCLUDE_GUARD

namespace sll 
{
	/*
		Basic benchmark for the SList class.
		The different tests perform the following operations:

		(o) TEST 01 :	SList()
						empty()
						size()
						push_front()
						push_back()
						pop_front()
						pop_back()

		(o) TEST 02 :	SList(n, const T& = T())
						SList(const SList&)

		(o) TEST 03 :	SList()
						push_front()
						empty()
						front()
						back()

		(o) TEST 04 :	SList(n, const T& = T())
						size()
						insert(pos, const T&)
						front()
						back()						
	*/
	void SListBasicTest(bool, bool, bool, bool);
}

#endif // SLISTBASICTEST_H_INCLUDE_GUARD