#ifndef SLIST_INLINES_H_INCLUDE_GUARD
#define SLIST_INLINES_H_INCLUDE_GUARD

#include <algorithm>
#include <iterator>
#include <cassert>

namespace sll
{
	#pragma region Iterators

	template<typename T, typename A>
	inline 
	typename SList<T, A>::iterator SList<T, A>::begin()
	{
		return iterator(m_head);
	}

	template<typename T, typename A>
	inline 
	typename SList<T, A>::const_iterator SList<T, A>::begin() const
	{
		return const_iterator(m_head);
	}

	template<typename T, typename A>
	inline 
	typename SList<T, A>::iterator SList<T, A>::end()
	{
		return iterator(0);
	}

	template<typename T, typename A>
	inline 
	typename SList<T, A>::const_iterator SList<T, A>::end() const
	{
		return const_iterator(0); 
	}

	#pragma endregion

	#pragma region Constructors

	template<typename T, typename A>
	inline
	SList<T, A>::SList(const A& a)
		: m_alloc(a), m_head(0), m_size(0) { }

	template<typename T, typename A>
	inline
	SList<T, A>::SList(size_type n, const T& value, const A& a)
		: m_alloc(a), m_head(0), m_size(0) 
	{
		for (size_type i = 0; i < n; ++i)
			push_front(value);
	}

	template<typename T, typename A>
	inline
	SList<T, A>::SList(const SList& l)
		: m_alloc(l.m_alloc), m_head(0), m_size(0)
	{
		iterator it = begin();
		std::copy(l.begin(), l.end(), std::inserter(*this, it));

		// Or, eq.: 
		// for (SList::const_iterator it = l.begin(); it != l.end(); ++it)
		// push_front(*it);
	}

	template<typename T, typename A>
	inline
	SList<T, A>& SList<T, A>::operator=(const SList& l)
	{
		if (this == &l) return *this;

		clear();
		iterator it = begin();
		std::copy(l.begin(), l.end(), std::inserter(*this, it));
	}

	// template<typename T, typename A>
	// template<typename In>
	// inline
	// SList<T, A>::SList(In first, In last, const A& a = A())
	//	 : _alloc(a), _head(0), _size(0)
	// {
	//	 iterator it = begin();
	//	 std::copy(first, last, std::inserter(*this, it));
	// }

	template<typename T, typename A>
	inline 
	SList<T, A>::~SList()
	{
		clear();
	}

	#pragma endregion

	#pragma region Access to elements

	template<typename T, typename A>
	inline 
	typename SList<T, A>::reference SList<T, A>::front()
	{
		assert(!empty());
		return *begin();
	}

	template<typename T, typename A>
	inline
	typename SList<T, A>::const_reference SList<T, A>::front() const
	{
		assert(!empty());
		return *begin();
	}

	template<typename T, typename A>
	inline
	typename SList<T, A>::reference SList<T, A>::back()
	{
		assert(!empty());
		iterator tail = get_tail();
		return *tail;
	}

	template<typename T, typename A>
	inline 
	typename SList<T, A>::const_reference SList<T, A>::back() const
	{
		assert(!empty());
		iterator tail = get_tail();
		return *tail;
	}

	#pragma endregion

	#pragma region Stack/Queue operations

	template<typename T, typename A>
	inline
	void SList<T, A>::push_front(const T& value)
	{
		m_head = allocate_and_create(value, m_head);
		++m_size;
	}

	template<typename T, typename A>
	inline 
	void SList<T, A>::pop_front()
	{
		assert(!empty());
		m_head = destroy_and_deallocate(m_head);
		--m_size;
	}

	#pragma endregion

	#pragma region List operations

	template<typename T, typename A>
	inline
	void SList<T, A>::clear()
	{
		erase(begin(), end());
	}

	#pragma endregion

	#pragma region Size and capacity

	template<typename T, typename A>
	inline 
	typename SList<T, A>::size_type SList<T, A>::size() const
	{
		return m_size;
	}

	template<typename T, typename A>
	inline
	bool SList<T, A>::empty() const 
	{
		return (begin() == end());
	}

	#pragma endregion

	#pragma region Utilities

	template<typename T, typename A>
	inline 
	typename SList<T, A>::allocator_type SList<T, A>::get_allocator() const
	{
		return m_alloc;
	}

	template<typename T, typename A>
	inline
	void SList<T, A>::swap(SList& l)
	{
		std::swap(m_alloc,	l.m_alloc);
		std::swap(m_head,	l.m_head);
		std::swap(m_size,	l.m_size);
	}

	#pragma endregion
}

#endif // SLIST_INLINES_H_INCLUDE_GUARD