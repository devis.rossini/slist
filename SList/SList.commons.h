#ifndef SLIST_COMMONS_H_INCLUDE_GUARD
#define SLIST_COMMONS_H_INCLUDE_GUARD

namespace sll
{
	template<bool flag, typename IsTrue, typename IsFalse>
	struct _const_behavior;

	template<typename IsTrue, typename IsFalse>
	struct _const_behavior<true, IsTrue, IsFalse> {
		typedef IsTrue type;
	};

	template<typename IsTrue, typename IsFalse>
	struct _const_behavior<false, IsTrue, IsFalse> {
		typedef IsFalse type;
	};
}

#endif // SLIST_COMMONS_H_INCLUDE_GUARD