#ifndef FIXEDSLIST_INLINES_H_INCLUDE_GUARD
#define FIXEDSLIST_INLINES_H_INCLUDE_GUARD

#include <algorithm>
#include <iterator>
#include <cassert>

namespace sll
{
	#pragma region Iterators

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::iterator FixedSList<T, N, A>::begin()
	{
		return iterator(m_lst, m_head);
	}

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::const_iterator FixedSList<T, N, A>::begin() const
	{
		return iterator(m_lst, m_head);
	}

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::iterator FixedSList<T, N, A>::end()
	{
		return iterator(m_lst, N);
	}

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::const_iterator FixedSList<T, N, A>::end() const
	{
		return iterator(m_lst, N);
	}

	#pragma endregion

	#pragma region Constructors and destructor

	template<typename T, unsigned int N, typename A>
	inline
	FixedSList<T, N, A>::FixedSList(const A& a)
		: m_alloc(a), m_lst(empty_list()), m_head(N), m_free(0) { }

	template<typename T, unsigned int N, typename A>
	inline
	FixedSList<T, N, A>::FixedSList(size_type n, const T& value, const A& a)
		: m_alloc(a), m_lst(empty_list()), m_head(N), m_free(0)
	{
		assert(n < N);

		for (size_type i = 0; i < n; ++i)
			push_front(value);
	}

	template<typename T, unsigned int N, typename A>
	inline
	FixedSList<T, N, A>::FixedSList(const FixedSList& l)
		: m_alloc(l.m_alloc), m_lst(empty_list()), m_head(N), m_free(0)
	{
		iterator it = begin();
		std::copy(l.begin(), l.end(), std::inserter(*this, it));
	}

	template<typename T, unsigned int N, typename A>
	inline
	FixedSList<T, N, A>& FixedSList<T, N, A>::operator=(const FixedSList& l)
	{
		if (this == &l) return *this;

		clear();
		iterator it = begin();
		std::copy(l.begin(), l.end(), std::inserter(*this, it));
	}

	template<typename T, unsigned int N, typename A>
	inline
	FixedSList<T, N, A>::~FixedSList() 
	{
		if (m_lst)
		{
			for (size_type i = 0; i < N; ++i)
				m_alloc.destroy(m_lst + i);
			m_alloc.deallocate(m_lst, N);
		}
	}

	#pragma endregion

	#pragma region Access to elements

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::reference FixedSList<T, N, A>::front()
	{
		assert(!empty());
		return *begin();
	}

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::const_reference FixedSList<T, N, A>::front() const
	{
		assert(!empty());
		return *begin();
	}

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::reference FixedSList<T, N, A>::back()
	{
		assert(!empty());
		iterator tail = get_tail();
		return *tail;
	}

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::const_reference FixedSList<T, N, A>::back() const
	{
		assert(!empty());
		iterator tail = get_tail();
		return *tail;
	}

	#pragma endregion

	#pragma region List operations

	template<typename T, unsigned int N, typename A>
	inline
	void FixedSList<T, N, A>::clear()
	{
		erase(begin(), end());
	}

	#pragma endregion

	#pragma region Size and capacity

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::size_type FixedSList<T, N, A>::size() const
	{
		size_type sz = 0;

		const_iterator it = begin();
		while(it != end())
		{
			++sz;
			++it;
		}
		return sz;
	}

	template<typename T, unsigned int N, typename A>
	inline
	bool FixedSList<T, N, A>::empty() const
	{
		return (begin() == end());
	}

	#pragma endregion

	#pragma region Utilities

	template<typename T, unsigned int N, typename A>
	inline
	void FixedSList<T, N, A>::swap(FixedSList& l)
	{
		std::swap(m_alloc,	l.m_alloc);
		std::swap(m_lst,	l.m_lst);
		std::swap(m_head,	l.m_head);
		std::swap(m_free,	l.m_free);
	}

	#pragma endregion

	#pragma region Private utilities

	template<typename T, unsigned int N, typename A>
	inline
	typename FixedSList<T, N, A>::node_ptr FixedSList<T, N, A>::empty_list()
	{
		node_ptr lst = m_alloc.allocate(N);

		for (size_type i = 0; i < N; ++i)
		{
			node_type empty_node(0, i + 1);
			m_alloc.construct(lst + i, empty_node);
		}

		return lst;
	}

	#pragma endregion
}

#endif // FIXEDSLIST_INLINES_H_INCLUDE_GUARD