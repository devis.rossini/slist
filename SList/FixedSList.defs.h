#ifndef FIXEDSLIST_DEFS_H_INCLUDE_GUARD
#define FIXEDSLIST_DEFS_H_INCLUDE_GUARD

#include "SList.commons.h"

#include <iterator>

namespace sll
{
	template<typename T>
	struct fixedslist_node
	{
		typedef T		value_type;
		typedef size_t	size_type;

		fixedslist_node(const T& v, size_type n)
			: value(v), next(n) { }

		value_type value;
		size_type next;
	};

	template <typename T, bool isconst = false> 
	struct fixedslist_iterator {

		typedef std::forward_iterator_tag								iterator_category;

		typedef T														value_type;

		typedef size_t													difference_type;

		typedef typename _const_behavior<isconst, const T&, T&>::type	reference;
		typedef typename _const_behavior<isconst, const T*, T*>::type	pointer;

		typedef typename _const_behavior<isconst, const fixedslist_node<T>&,    
			fixedslist_node<T>&>::type node_ref;

		typedef typename _const_behavior<isconst, const fixedslist_node<T>*,    
			fixedslist_node<T>*>::type node_ptr;

		typedef size_t													index_type;
		
		fixedslist_iterator(node_ptr l = 0, index_type i = 0)
			: lst(l), index(i) { }

		fixedslist_iterator(const fixedslist_iterator<T, false>& it) 
			: lst(it.lst), index(it.index) { }

		reference operator*() const 
		{ 
			return get_node().value;
		}

		pointer operator->() const 
		{ 
			return &(get_node().value); 
		}

		fixedslist_iterator& operator++() 
		{ 
			index = get_node().next; 
			return *this; 
		}

		fixedslist_iterator operator++(int) 
		{
			fixedslist_iterator tmp(*this);
			++*this;
			return tmp;
		}

		node_ref get_node() const
		{
			return lst[index];
		}

		index_type get_index() const
		{
			return index;
		}

		friend bool operator==(const fixedslist_iterator& it1, const fixedslist_iterator& it2) 
		{
			return (it1.index == it2.index);
		}

		friend bool operator!=(const fixedslist_iterator& it1, const fixedslist_iterator& it2) 
		{
			return (it1.index != it2.index);
		}

		node_ptr lst;
		index_type index;
	};
}

#endif // FIXEDSLIST_DEFS_H_INCLUDE_GUARD