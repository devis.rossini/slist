#ifndef FIXEDSLIST_TEMPLATES_H_INCLUDE_GUARD
#define FIXEDSLIST_TEMPLATES_H_INCLUDE_GUARD

#include <cassert>

namespace sll
{
	#pragma region Stack/Queue operations

	template<typename T, unsigned int N, typename A>
	void FixedSList<T, N, A>::push_front(const T& value)
	{
		assert(m_free != N);

		size_type tmp = m_lst[m_free].next;
		m_lst[m_free] = node_type(value, m_head);
		m_head = m_free;
		m_free = tmp;
	}

	template<typename T, unsigned int N, typename A>
	void FixedSList<T, N, A>::pop_front()
	{
		assert(!empty());

		size_type tmp = m_lst[m_head].next;
		m_lst[m_head].next = m_free;
		m_free = m_head;
		m_head = tmp;
	}

	template<typename T, unsigned int N, typename A>
	void FixedSList<T, N, A>::push_back(const T& value)
	{
		assert(m_free != N);

		if (empty())
		{
			push_front(value);
		}
		else 
		{
			insert(end(), value);
		}
	}

	template<typename T, unsigned int N, typename A>
	void FixedSList<T, N, A>::pop_back()
	{
		assert(!empty());

		if (size() == 1)
		{
			pop_front();
		}
		else 
		{
			iterator tail = get_tail();
			erase(tail);
		}
	}

	#pragma endregion

	#pragma region List operations

	template<typename T, unsigned int N, typename A>
	typename FixedSList<T, N, A>::iterator FixedSList<T, N, A>::insert(iterator pos, const T& value)
	{
		assert(m_free != N);

		if (pos == begin())
		{
			push_front(value);
			return begin();
		}
		else 
		{
			iterator prev = begin();
			iterator curr = prev;
			++curr;

			while (curr != pos)
			{
				++prev; 
				++curr;
			}

			size_type insertion_index = m_free;

			size_type tmp = m_lst[m_free].next;
			m_lst[m_free] = node_type(value, curr.get_index());
			prev.get_node().next = m_free;
			m_free = tmp;

			return iterator(m_lst, insertion_index);
		}

		return end();
	}

	template<typename T, unsigned int N, typename A>
	typename FixedSList<T, N, A>::iterator FixedSList<T, N, A>::erase(iterator pos)
	{
		if (pos == end())
			return end();

		if (pos == begin())
		{
			pop_front();
			return begin();
		}

		iterator prev = begin();
		iterator curr = prev;
		++curr;

		while (curr != pos)
		{
			++prev;
			++curr;
		}

		size_type next = curr.get_node().next;

		prev.get_node().next = next;
		curr.get_node().next = m_free;
		m_free = curr.get_index();

		return iterator(m_lst, next);
	}

	template<typename T, unsigned int N, typename A>
	typename FixedSList<T, N, A>::iterator FixedSList<T, N, A>::erase(iterator first, iterator last)
	{
		iterator ret = end();
		iterator tmp = first;

		while (tmp != last)
		{
			iterator next = tmp;
			++next;

			ret = erase(tmp);
			tmp = next;
		}

		return ret;
	}

	#pragma endregion

	#pragma region Private utilities

	template<typename T, unsigned int N, typename A>
	typename FixedSList<T, N, A>::iterator FixedSList<T, N, A>::get_tail()
	{
		iterator tail = begin();

		if (tail != end())
		{
			iterator next = tail;
			++next;

			while (next != end())
			{
				++tail;
				++next;
			}
		}

		return tail;
	}

	#pragma endregion
}

#endif // FIXEDSLIST_TEMPLATES_H_INCLUDE_GUARD